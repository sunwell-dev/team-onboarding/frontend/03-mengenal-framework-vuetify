### Soal Studi Kasus: Pembuatan Dashboard Sederhana dengan Vuetify dan multi layout
membuat sebuah dashboard sederhana menggunakan Vuetify. Berikut adalah detail dari studi kasus tersebut:

1. **Header dan Drawer:**
   - Desain sebuah header yang menampilkan judul aplikasi di bagian kiri dan ikon untuk membuka drawer di bagian kanan.
   - Buatlah sebuah drawer (sidebar) yang menampilkan menu navigasi untuk berpindah antar halaman.

2. **Konten Utama yang Dinamis:**
   - Konten utama dashboard akan bergantung pada menu yang dipilih pada drawer.
   - Saat sebuah menu dipilih, konten utama akan berubah untuk menampilkan halaman yang sesuai dengan menu tersebut.

3. **Tampilan Umum:**
   - Pastikan tata letak dan desain dashboard terlihat estetis dan responsif.
   - Gunakan komponen Vuetify seperti `v-app-bar`, `v-navigation-drawer`, dan lainnya sesuai kebutuhan untuk menyusun tampilan dashboard.

4. **Kemampuan Navigasi:**
   - Pastikan pengguna dapat berpindah antar halaman dengan mudah menggunakan menu navigasi pada drawer.
   - Header harus memiliki fungsi untuk membuka dan menutup drawer.

5. Halaman Blog sederhana tanpa drawer dengan layout yang berbeda

### Catatan Tambahan:

- Responsivitas sangat penting, pastikan dashboard dapat diakses dengan baik pada berbagai perangkat.
- Fokuslah pada kebutuhan dasar dashboard: header, drawer, dan konten utama yang dinamis.



