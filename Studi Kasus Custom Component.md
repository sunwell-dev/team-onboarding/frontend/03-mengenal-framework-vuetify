## Kasus: Membuat Custom Component dengan Vue untuk Input Nomor Handphone

### Deskripsi Kasus
Membuat sebuah komponen kustom dengan Vue untuk memasukkan nomor handphone. Komponen ini harus mematuhi aturan berikut:

1. Panjang nomor handphone minimal 10 hingga maksimal 13 angka.
2. Nomor handphone dapat dimulai dengan 089, +62, atau 62.
3. Pengguna dapat mengatur apakah input diperlukan atau tidak.
4. Pengguna dapat memasukkan kode negara selain kode Indonesia (opsional).

### Fungsionalitas yang Dibutuhkan
- Validasi panjang nomor handphone.
- Validasi format nomor handphone yang diperbolehkan.
- Kemampuan untuk menandai input sebagai wajib diisi (required) atau opsional.
- Mendukung beberapa format input nomor handphone (mulai dengan 089, +62, atau 62).
- Kemampuan untuk menyesuaikan teks pesan kesalahan melalui slot.

# Format Keluaran
contoh: 6280965855347
- harus dengan kode negara
- tanpa simbol

### Langkah-langkah Implementasi

1. **Validasi Panjang Nomor Handphone:**
   - Pastikan panjang nomor handphone minimal 10 dan maksimal 13 angka.
   - Tampilkan pesan kesalahan jika panjang tidak memenuhi kriteria.

2. **Validasi Format Nomor Handphone:**
   - Cek apakah nomor handphone dimulai dengan 089, +62, atau 62.
   - Izinkan kode negara lain jika disetel oleh pengguna.

3. **Input yang Dapat Diperlukan atau Opsional:**
   - Tambahkan properti untuk menandai apakah input nomor handphone diperlukan atau opsional.
   - Sesuaikan tampilan komponen untuk menunjukkan status kewajiban input.

4. **Mendukung Beberapa Format Input Nomor Handphone:**
   - Berikan kemampuan kepada pengguna untuk memasukkan nomor handphone dengan format yang berbeda (mulai dengan 089, +62, atau 62).
   - Sesuaikan validasi sesuai dengan format yang diberikan.

### Contoh Output

Jika pengguna memasukkan nomor handphone dengan format yang valid, komponen akan mengembalikan nomor handphone dalam format yang konsisten, misalnya: `6280965855347`.

Jika pengguna tidak memasukkan nomor handphone atau memasukkan nomor handphone yang tidak valid, tampilkan pesan kesalahan yang sesuai.

### Pesan Kesalahan

1. **Panjang Nomor Handphone Tidak Valid:**
   - "Nomor handphone harus terdiri dari 10 hingga 13 angka."

2. **Nomor Handphone Diperlukan (Jika Input Wajib):**
   - "Nomor handphone diperlukan. Mohon masukkan nomor handphone Anda."
3. **Kode Negara Tidak Dikenali:**
   - "Mohon masukkan nomor handphone dengan kode negara yang dikenali (misalnya +62 untuk Indonesia)."
